<?php
//array combine
echo "<pre/>";
$a = array('green', 'red', 'yellow');
$b = array('avocado', 'apple', 'banana');
$c = array_combine($a, $b);

print_r($c);

?>


<?php
echo "<pre/>";
$array1 = array("color" => "red", 2, 4);
$array2 = array("a", "b", "color" => "green", "shape" => "trapezoid", 4);
$result = array_merge($array1, $array2);
print_r($result);
?>
<?php
//array_key_exists

$search_array = array('first' => 1, 'second' => 4);
if (array_key_exists('first', $search_array)) {
    echo "The 'first' element is in the array";
}
?>

<?php
echo "<pre/>";
$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(0 => "pineapple", 4 => "cherry");
$replacements2 = array(0 => "grape" , 1=>"guava");

$basket = array_replace($base, $replacements,$replacements2);
print_r($basket);
?>

<?php
//compact
echo "<pre/>";
$city  = "San Francisco";
$state = "CA";
$event = "SIGGRAPH";

$location_vars = array("city", "state");

$result = compact("event", "state", $location_vars);
print_r($result);
?>

<?php
//asort()
echo "<pre/>";
$age = array("peter" => "40", "ben" => "37", "joe" => "50");
asort($age);
foreach ($age as $key => $val) {
    echo "$key = $val\n";
}
?>

<?php
//array_rand()
echo "<pre/>";
$input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
$rand_keys = array_rand($input, 3);//ican echo as many as elemnt declared as offset if the value isonly offse
echo $input[$rand_keys[1]] . "\n";// if the $rand_keys[value>offset-1] it will show error]
echo $input[$rand_keys[2]] . "\n";
echo $input[$rand_keys[0]] . "\n";
?>

<?php
//range()
// array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
echo "<br/>";
foreach (range(0, 12) as $number) {
    echo $number;
}
echo "<br/>";
foreach (range(0, 100, 10) as $number) {
    echo $number;
}
?>

<?php
$input = array(12, 10, 9);

$result = array_pad($input, 7, 12);
// result is array(12, 10, 9, 0, 0)
print_r($result);

//$result = array_pad($input, -7, -1);
// result is array(-1, -1, -1, -1, 12, 10, 9)

//$result = array_pad($input, 2, "noop");
// not padded
?>
